package com.demo.mybatis.books.model;

import javax.validation.constraints.NotNull;

public class BookInfoRequest {
	@NotNull
	private String author;
	
	@NotNull
	private String title;
	
	@NotNull
	private int published;
	
	@NotNull
	private String remark;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPublished() {
		return published;
	}

	public void setPublished(int published) {
		this.published = published;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
