package com.demo.mybatis.books.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.demo.mybatis.books.mapper.BooksMapper;
import com.demo.mybatis.books.model.BookInfoRequest;
import com.demo.mybatis.books.model.BookInfoResponse;

@Service
public class BooksServiceImpl implements BooksService {

	@Autowired
	private BooksMapper bookMapper;

	@Override
	public List<BookInfoResponse> getBooks(String title, String author) {
		return bookMapper.findAll(title, author);
	}

	@Override
	public int addBook(BookInfoRequest req) {
		return bookMapper.addBook(req);
	}

}
