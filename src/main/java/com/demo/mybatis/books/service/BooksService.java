package com.demo.mybatis.books.service;

import java.util.List;

import com.demo.mybatis.books.model.*;


public interface BooksService {
	public List<BookInfoResponse> getBooks(String title, String author);
	
	public int addBook(BookInfoRequest req);

 }
