package com.demo.mybatis.books.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import com.demo.mybatis.books.model.*;

@Repository
public interface BooksMapper {
	
	public List<BookInfoResponse> findAll(@Param("title") String appId, @Param("author") String groupId);
	
	public int addBook(BookInfoRequest req);

}
